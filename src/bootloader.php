<?php
/**
 Copyright (c) 2015, VOOV LLC.
 All rights reserved.
 Written by Daniel Fekete
*/

require 'vendor/autoload.php';
require 'deployer/Deploy.php';

use Symfony\Component\Console\Application;


$application = new Application();
$application->add(new \Deployer\Deploy());
$application->run();