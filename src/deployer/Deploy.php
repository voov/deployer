<?php
/**
 Copyright (c) 2015, VOOV LLC.
 All rights reserved.
 Written by Daniel Fekete
*/

namespace Deployer;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AFM\Rsync\Rsync;

class Deploy extends Command {
	protected function configure() {
		$this->setName('deploy')
			->setDescription('Deploy application')
			->addArgument("task", InputArgument::REQUIRED)
			->addOption('test', null, InputOption::VALUE_NONE)
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output)  {
		$options = require $input->getArgument("task");
		var_dump($options);

	}
}