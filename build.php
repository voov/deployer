<?php
/**
 Copyright (c) 2015, VOOV LLC.
 All rights reserved.
 Written by Daniel Fekete
*/
ini_set('phar.readonly', 0);
$phar = new Phar("build/deployer.phar",
				 FilesystemIterator::CURRENT_AS_FILEINFO |
				 FilesystemIterator::KEY_AS_FILENAME, "deployer.phar");

$phar->buildFromDirectory("src",'/.php$/');
$phar->setStub("#!/usr/bin/env php\n<?php require 'phar://deployer.phar/bootloader.php'; __HALT_COMPILER(); ?>");